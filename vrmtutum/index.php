<html lang="en">
	<head>

		<title>VRM TUTUM</title>

		<meta charset="utf-8">
		<meta name="description" content="VRM Core">
		<meta name="author" content="Criteria">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">


		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/login.css" rel="stylesheet">
		<link href="css/animate.css" rel="stylesheet">
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/login.js"></script>

		<!-- sweetAlert -->
		<script src="js/sweetalert/src/SweetAlert.js"></script>
		<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
		<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
		<link rel="stylesheet" href="js/sweetalert/dist/sweetalert2.min.css">
		<script type="text/javascript">

							var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
							(function(){
							var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
							s1.async=true;
							s1.src="https://embed.tawk.to/5e0e6c107e39ea1242a2ca8a/default?currentuser=pascual";
							s1.charset="UTF-8";
							s1.setAttribute("crossorigin","*");
							s0.parentNode.insertBefore(s1,s0);
							})();

							Tawk_API = Tawk_API || {};
							usuarioChat = "Visitante";
							correoChat = "Visitante";
							Tawk_API.visitor = {name : usuarioChat, email : correoChat};
						
					</script>
	</head>

    <body class="ng-tns-0-0 theme-default">
		<div id="loading">
		  <img id="loading-image" src="img/criteria.svg" />
		  <div class="spinnerWrapper">
			   <div class="spinnerOuter"></div>
			   <div class="spinnerMiddle"></div>
			</div>
		</div>

		<div id="dvForm" class="dvContainer noMostrar animated">
			<div>
				<img class="logo" src="img/tutum.png">
			</div>
			<div class="title">Portal de Proveedores VRM</div>

			<div class="form__group">
			  <input type="text" name="txtClave" id="txtClave" class="form__field" placeholder="Clave">
			  <label for="txtClave" class="form__label">Clave</label>
			</div>

			<div class="form__group">
			  <input type="email" name="txtEmail" id="txtEmail" class="form__field" placeholder="Your Email">
			  <label for="txtEmail" class="form__label">Email</label>
			</div>

			<div class="form__group">
			  <input type="password" name="txtPassword" id="txtPassword" class="form__field" placeholder="Your Email">
			  <label for="txtPassword" class="form__label">Password</label>
			</div>

			<br>
			<div>
				<a id="btnLogin" href="#" class="fancy-button medium blueRiver vertical">
					Ingresar
					<span class="icon">
						<i class="fa fa-arrow-circle-up"></i>
					</span>
				</a>
			</div>

			<div>
				<a id="btnSignIn" href="prospectoReg.php">
					<h2>
						Registro de Prospectos
					</h2>
				</a>
			</div>
			
			<div>
				<a id="btnSignIn" href="pages/ActivacionNueProv/ActivarNProv.php">
					<h2>
						Activación de Proveedores
					</h2>
				</a>
			</div>
			
			<div>
				<a id="btnSignIn" href="pages/recuperaContrasena/RecuperaContra.php">
					<h2>
						¿Olvidaste tu Contraseña?
					</h2>
				</a>
			</div>
		</div>

	</body>
</html>
